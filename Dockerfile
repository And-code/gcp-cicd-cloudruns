FROM golang:bullseye

#RUN apt-get -y update
#RUN apt-get -y install apache2
#
#RUN echo 'Docker Image on CloudRun!<br>'   > /var/www/html/index.html
#RUN echo '<b><font color="magenta">Version 1.1</font></b>' >> /var/www/html/index.html
#
#CMD ["/usr/sbin/apache2ctl", "-D","FOREGROUND"]
#WORKDIR /app

COPY . .

RUN GOOS=linux GOARCH=amd64 go build -gcflags "all=-N -l" -o bin/app ./

EXPOSE 8080

CMD ["./bin/app"]